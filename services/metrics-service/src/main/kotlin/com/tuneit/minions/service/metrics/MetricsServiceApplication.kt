package com.tuneit.minions.service.metrics

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MetricsServiceApplication

fun main(args: Array<String>) {
    runApplication<MetricsServiceApplication>(*args)
}
